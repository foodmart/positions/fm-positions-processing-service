package fm.positions.processing.controller.impl;

import fm.common.core.controller.BaseController;
import fm.common.core.model.response.ResponseCoreEntity;
import fm.positions.processing.controller.doc.PositionsDocs;
import fm.positions.processing.model.request.BatchPositionsByIdsRequest;
import fm.positions.processing.model.request.SearchRequest;
import fm.positions.processing.model.response.PositionResponse;
import fm.positions.processing.model.response.PositionsIdsResponse;
import fm.positions.processing.model.response.PositionsResponse;
import fm.positions.processing.service.api.PositionsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class PositionsController extends BaseController implements PositionsDocs {

    private final PositionsService positionsService;

    @Override
    public ResponseCoreEntity<PositionResponse> findPositionById(int positionId) {
        return response(positionsService.findPositionById(positionId));
    }

    @Override
    public ResponseCoreEntity<PositionsResponse> findPositionsByIds(BatchPositionsByIdsRequest request) {
        return response(positionsService.findPositionsByIds(request.getPositionsIds()));
    }

    @Override
    public ResponseCoreEntity<PositionsResponse> findPositions(SearchRequest request) {
        return response(positionsService.findPositions(request));
    }

    @Override
    public ResponseCoreEntity<PositionsIdsResponse> findPositionsIds(SearchRequest request) {
        return response(positionsService.findPositionsIds(request));
    }
}
