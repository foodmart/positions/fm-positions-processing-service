package fm.positions.processing.controller.doc;

import fm.common.core.model.annoation.CommonParams;
import fm.common.core.model.annoation.Pageable;
import fm.common.core.model.response.ResponseCoreEntity;
import fm.positions.processing.model.request.BatchPositionsByIdsRequest;
import fm.positions.processing.model.request.SearchRequest;
import fm.positions.processing.model.response.PositionResponse;
import fm.positions.processing.model.response.PositionsIdsResponse;
import fm.positions.processing.model.response.PositionsResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface PositionsDocs {

    @GetMapping("/{position_id}")
    @CommonParams
    ResponseCoreEntity<PositionResponse> findPositionById(@PathVariable("position_id") int positionId);

    @PostMapping("/batch")
    @CommonParams
    ResponseCoreEntity<PositionsResponse> findPositionsByIds(@RequestBody BatchPositionsByIdsRequest request);

    @PostMapping("/search")
    @CommonParams
    @Pageable
    ResponseCoreEntity<PositionsResponse> findPositions(@RequestBody SearchRequest request);

    @PostMapping("/search/ids")
    @CommonParams
    ResponseCoreEntity<PositionsIdsResponse> findPositionsIds(@RequestBody SearchRequest request);


}
