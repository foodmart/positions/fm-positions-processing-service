package fm.positions.processing.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PositionResponse {

    private int positionId;

    private String positionTitle;

    private String paymentType;

    private int minScale;

    private int maxScale;

    private String managementRole;

}
