package fm.positions.processing.model.request;

import lombok.Data;

import java.util.List;

@Data
public class BatchPositionsByIdsRequest {

    private List<Integer> positionsIds;
}
