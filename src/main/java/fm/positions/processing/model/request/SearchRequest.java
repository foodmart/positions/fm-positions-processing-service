package fm.positions.processing.model.request;

import fm.positions.processing.model.type.StringType;
import fm.positions.processing.model.type.Type;
import lombok.Data;

@Data
public class SearchRequest {

    private StringType positionTitle;

    private StringType paymentType;

    private Type<Integer> minScale;

    private Type<Integer> maxScale;

    private StringType managementRole;
}
