package fm.positions.processing.model.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import static fm.positions.processing.constant.PositionEntityConstants.COLLECTION_NAME;
import static fm.positions.processing.constant.PositionEntityConstants.Fields.*;

@Document(collection = COLLECTION_NAME)
@Data
public class Position {

    @Id
    private String id;

    @Field(name = POSITION_ID_FIELD_NAME)
    private int positionId;

    @Field(name = POSITION_TITLE_FIELD_NAME)
    private String positionTitle;

    @Field(name = PAYMENT_TYPE_FIELD_NAME)
    private String paymentType;

    @Field(name = MIN_SCALE_FIELD_NAME)
    private int minScale;

    @Field(name = MAX_SCALE_FIELD_NAME)
    private int maxScale;

    @Field(name = MANAGEMENT_ROLE_FIELD_NAME)
    private String managementRole;
}
