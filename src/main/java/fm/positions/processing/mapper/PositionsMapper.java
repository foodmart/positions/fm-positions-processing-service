package fm.positions.processing.mapper;

import fm.common.core.api.mapper.Mapper;
import fm.positions.processing.model.response.PositionResponse;
import fm.positions.processing.model.response.PositionsResponse;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PositionsMapper implements Mapper<List<PositionResponse>, PositionsResponse> {
    @Override
    public PositionsResponse map(List<PositionResponse> positionResponses) {
        return PositionsResponse.builder()
                .positions(positionResponses)
                .build();
    }
}
