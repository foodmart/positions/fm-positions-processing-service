package fm.positions.processing.mapper;

import fm.common.core.api.mapper.Mapper;
import fm.positions.processing.model.entity.Position;
import fm.positions.processing.model.response.PositionResponse;
import org.springframework.stereotype.Component;

@Component
public class PositionMapper implements Mapper<Position, PositionResponse> {
    @Override
    public PositionResponse map(Position position) {
        return PositionResponse.builder()
                .positionId(position.getPositionId())
                .positionTitle(position.getPositionTitle())
                .managementRole(position.getManagementRole())
                .paymentType(position.getPaymentType())
                .maxScale(position.getMaxScale())
                .minScale(position.getMinScale())
                .managementRole(position.getManagementRole())
                .build();
    }
}
