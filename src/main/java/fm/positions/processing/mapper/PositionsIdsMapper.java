package fm.positions.processing.mapper;

import fm.common.core.api.mapper.Mapper;
import fm.positions.processing.model.response.PositionsIdsResponse;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PositionsIdsMapper implements Mapper<List<Integer>, PositionsIdsResponse> {
    @Override
    public PositionsIdsResponse map(List<Integer> positionsIds) {
        return PositionsIdsResponse.builder()
                .positionsIds(positionsIds)
                .build();
    }
}
