package fm.positions.processing.repository.api;

import fm.positions.processing.model.request.SearchRequest;
import org.springframework.data.mongodb.core.query.Query;

public interface QueryBuilder {

    Query build(SearchRequest request);
}
