package fm.positions.processing.repository.api;

import fm.positions.processing.model.entity.Position;
import fm.positions.processing.model.request.SearchRequest;

import java.util.List;
import java.util.Optional;

public interface PositionsRepository {

    Optional<Position> findPositionById(int positionId);

    List<Position> findPositionsByIds(List<Integer> positionsIds);

    List<Position> findPositions(SearchRequest searchRequest);

    List<Position> findPositions(SearchRequest searchRequest, int page, int size);

}
