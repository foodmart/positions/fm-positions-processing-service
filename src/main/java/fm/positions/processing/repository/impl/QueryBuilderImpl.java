package fm.positions.processing.repository.impl;

import fm.positions.processing.model.request.SearchRequest;
import fm.positions.processing.model.type.LocalDateTimeType;
import fm.positions.processing.model.type.StringType;
import fm.positions.processing.model.type.Type;
import fm.positions.processing.repository.api.QueryBuilder;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.time.ZoneOffset;

import static fm.positions.processing.constant.PositionEntityConstants.Fields.*;

@Component
public class QueryBuilderImpl implements QueryBuilder {
    @Override
    public Query build(SearchRequest request) {
        Criteria criteria = new Criteria();
        addTypeCriteria(criteria, request.getPositionTitle(), POSITION_TITLE_FIELD_NAME);
        addTypeCriteria(criteria, request.getPaymentType(), PAYMENT_TYPE_FIELD_NAME);
        addTypeCriteria(criteria, request.getMinScale(), MIN_SCALE_FIELD_NAME);
        addTypeCriteria(criteria, request.getMaxScale(), MAX_SCALE_FIELD_NAME);
        addTypeCriteria(criteria, request.getManagementRole(), MANAGEMENT_ROLE_FIELD_NAME);
        return Query.query(criteria);
    }

    private void addTypeCriteria(Criteria criteria, Type<?> type, String fieldName) {
        if (type != null) {
            if (type instanceof StringType stringType) {
                if (stringType.isExact()) {
                    criteria.and(fieldName).is(stringType.getValue());
                } else {
                    criteria.and(fieldName).regex(stringType.getValue());
                }
            } else if (type instanceof LocalDateTimeType dateTimeType) {
                criteria.and(fieldName).is(dateTimeType.getValue().atZone(ZoneOffset.systemDefault()).toInstant());
            } else {
                criteria.and(fieldName).is(type.getValue());
            }
        }
    }
}
