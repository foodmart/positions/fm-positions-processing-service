package fm.positions.processing.repository.impl;

import fm.positions.processing.model.entity.Position;
import fm.positions.processing.model.request.SearchRequest;
import fm.positions.processing.repository.api.PositionsRepository;
import fm.positions.processing.repository.api.QueryBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import static fm.positions.processing.constant.PositionEntityConstants.Fields.POSITION_ID_FIELD_NAME;

@Repository
@RequiredArgsConstructor
public class PositionsRepositoryImpl implements PositionsRepository {

    private final MongoTemplate mongoTemplate;

    private final QueryBuilder queryBuilder;

    @Override
    public Optional<Position> findPositionById(int positionId) {
        Criteria criteria = Criteria.where(POSITION_ID_FIELD_NAME).is(positionId);
        Query query = Query.query(criteria);
        return mongoTemplate.find(query, Position.class).stream().findFirst();
    }

    @Override
    public List<Position> findPositionsByIds(List<Integer> positionsIds) {
        Criteria criteria = Criteria.where(POSITION_ID_FIELD_NAME).in(positionsIds);
        Query query = Query.query(criteria);
        return mongoTemplate.find(query, Position.class);
    }

    @Override
    public List<Position> findPositions(SearchRequest searchRequest) {
        Query query = queryBuilder.build(searchRequest);
        return mongoTemplate.find(query, Position.class);
    }

    @Override
    public List<Position> findPositions(SearchRequest searchRequest, int page, int size) {
        Query query = queryBuilder.build(searchRequest);
        Pageable pageable = PageRequest.of(page, size);
        query = query.with(pageable);
        return mongoTemplate.find(query, Position.class);
    }
}
