package fm.positions.processing.constant;

public class PositionEntityConstants {

    public static final String COLLECTION_NAME = "position";

    public static class Fields {

        public static final String POSITION_ID_FIELD_NAME = "position_id";
        public static final String POSITION_TITLE_FIELD_NAME = "position_title";
        public static final String PAYMENT_TYPE_FIELD_NAME = "pay_type";
        public static final String MIN_SCALE_FIELD_NAME = "min_scale";
        public static final String MAX_SCALE_FIELD_NAME = "max_scale";
        public static final String MANAGEMENT_ROLE_FIELD_NAME = "management_role";


    }
}
