package fm.positions.processing.service.impl;

import fm.common.core.api.mapper.MapperProvider;
import fm.common.core.model.header.CommonContext;
import fm.positions.processing.exception.PositionNotFoundException;
import fm.positions.processing.model.entity.Position;
import fm.positions.processing.model.request.SearchRequest;
import fm.positions.processing.model.response.PositionResponse;
import fm.positions.processing.model.response.PositionsIdsResponse;
import fm.positions.processing.model.response.PositionsResponse;
import fm.positions.processing.repository.api.PositionsRepository;
import fm.positions.processing.service.api.PositionsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PositionsServiceImpl implements PositionsService {

    private final PositionsRepository positionsRepository;

    private final MapperProvider mapperProvider;

    private final CommonContext commonContext;

    @Override
    public PositionResponse findPositionById(int positionId) {
        Optional<Position> positionOpt = positionsRepository.findPositionById(positionId);
        if (positionOpt.isPresent()) {
            return mapperProvider.map(positionOpt.get(), PositionResponse.class);
        }
        throw new PositionNotFoundException();
    }

    @Override
    public PositionsResponse findPositionsByIds(List<Integer> positionsIds) {
        return mapperProvider.map(positionsRepository.findPositionsByIds(positionsIds)
                .stream()
                .map(position -> mapperProvider.map(position, PositionResponse.class))
                .toList(), PositionsResponse.class);
    }

    @Override
    public PositionsIdsResponse findPositionsIds(SearchRequest searchRequest) {
        return mapperProvider.map(positionsRepository.findPositions(searchRequest)
                .stream()
                .map(Position::getPositionId)
                .toList(), PositionsIdsResponse.class);
    }

    @Override
    public PositionsResponse findPositions(SearchRequest searchRequest) {
        return mapperProvider.map(positionsRepository.findPositions(searchRequest, commonContext.getPage(),
                        commonContext.getSize())
                .stream()
                .map(position -> mapperProvider.map(position, PositionResponse.class))
                .toList(), PositionsResponse.class);
    }
}
