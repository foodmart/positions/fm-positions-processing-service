package fm.positions.processing.service.api;

import fm.positions.processing.model.request.SearchRequest;
import fm.positions.processing.model.response.PositionResponse;
import fm.positions.processing.model.response.PositionsIdsResponse;
import fm.positions.processing.model.response.PositionsResponse;

import java.util.List;

public interface PositionsService {

    PositionResponse findPositionById(int positionId);

    PositionsResponse findPositionsByIds(List<Integer> positionsIds);

    PositionsIdsResponse findPositionsIds(SearchRequest searchRequest);

    PositionsResponse findPositions(SearchRequest searchRequest);


}
